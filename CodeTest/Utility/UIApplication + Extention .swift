//
//  UIApplication + Extention .swift
//  CodeTest
//
//  Created by Faizul Karim on 30/10/21.
//

import Foundation
extension UIApplication {

    class var safeArea : UIEdgeInsets {
        if UIApplication.shared.windows.count > 0{
            if #available(iOS 11.0, *) {
                return UIApplication.shared.windows[0].safeAreaInsets
            } else {
                return UIEdgeInsets.zero
                // Fallback on earlier versions
            }
        }
        return UIEdgeInsets.zero
    }
}

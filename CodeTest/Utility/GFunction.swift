//
//  GFunction.swift
//  CodeTest
//  Created by Faizul Karim on 30/10/21.


import UIKit
import AudioToolbox
import AVKit
import MessageUI

class GFunction: NSObject {

    
    static let shared   : GFunction = GFunction()
    let activityIndicator = UIActivityIndicatorView(style: .white)
    let viewBGLoder: UIView = UIView()
    let snackbar: TTGSnackbar = TTGSnackbar()
    let snackBarNetworkReachability : TTGSnackbar = TTGSnackbar()
    
    enum ConvertType {
        case LOCAL,UTC,NOCONVERSION
    }
    //MARK:- Custom Alert
    
    func showSnackBar(_ message : String, duration : TTGSnackbarDuration = .short ,isError : Bool = false, animation : TTGSnackbarAnimationType = .slideFromTopBackToTop) {
        snackbar.message = message
        snackbar.duration = duration
        // Change the content padding inset
        snackbar.contentInset = UIEdgeInsets.init(top: UIApplication.safeArea.top + 20, left: 8, bottom: 8, right: 8)
        
        // Change margin
        snackbar.leftMargin = 0
        snackbar.rightMargin = 0
        snackbar.topMargin = -UIApplication.safeArea.top
        
        // Change message text font and color
        snackbar.messageTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        snackbar.messageTextFont = UIFont.systemFont(ofSize: 16.0)
        
        // Change snackbar background color
        snackbar.backgroundColor = .white
        
        snackbar.onTapBlock = { snackbar in
            snackbar.dismiss()
        }
        
        snackbar.onSwipeBlock = { (snackbar, direction) in
            
            // Change the animation type to simulate being dismissed in that direction
            if direction == .right {
                snackbar.animationType = .slideFromLeftToRight
            } else if direction == .left {
                snackbar.animationType = .slideFromRightToLeft
            } else if direction == .up {
                snackbar.animationType = .slideFromTopBackToTop
            } else if direction == .down {
                snackbar.animationType = .slideFromTopBackToTop
            }
            
            snackbar.dismiss()
        }
        
        // snackbar.cornerRadius = 0.0
        // Change animation duration
        snackbar.animationDuration = 0.5
        
        // Animation type
        snackbar.animationType = animation
        snackbar.show()
    }
    
    func showNoNetworkSnackBar(_ message : String = "No Internet Connection") {
        
        snackBarNetworkReachability.message = message
        snackBarNetworkReachability.duration = .forever
        // Change the content padding inset
        snackBarNetworkReachability.contentInset = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)
        
        // Change margin
        snackBarNetworkReachability.leftMargin = 0
        snackBarNetworkReachability.rightMargin = 0
        snackBarNetworkReachability.topMargin = 20
        
        // Change message text font and color
        snackBarNetworkReachability.messageTextColor = UIColor.white
        snackBarNetworkReachability.messageTextAlign = .center
        snackBarNetworkReachability.messageTextFont = UIFont.systemFont(ofSize: 12.0)
        
        // Change snackbar background color
        snackBarNetworkReachability.backgroundColor = #colorLiteral(red: 0.6039215686, green: 0.6274509804, blue: 0.6980392157, alpha: 1).withAlphaComponent(0.9)
        
        //        snackBarNetworkReachability.cornerRadius = 0.0
        // Change animation duration
        snackBarNetworkReachability.animationDuration = 0.5
        
        // Animation type
        snackBarNetworkReachability.animationType = .topSlideFromRightToLeft
        snackBarNetworkReachability.show()
    }
    
    func removeNoNetworkSnackBar(){
        
        snackBarNetworkReachability.dismiss()
    }
    func addLoader(_ message : String? = "Loading...") {
        removeLoader()
        
        self.viewBGLoder.frame = UIScreen.main.bounds
        self.viewBGLoder.backgroundColor = .clear
        self.viewBGLoder.tag = 1307966
        
        activityIndicator.center = UIApplication.shared.windows.first?.center ?? .zero
        activityIndicator.color = .green
        activityIndicator.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        
        self.viewBGLoder.addSubview(activityIndicator)
        UIApplication.shared.windows.first?.addSubview(viewBGLoder)
    }
    
    func removeLoader() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
        UIApplication.shared.windows.first?.viewWithTag(1307966)?.removeFromSuperview()
        self.viewBGLoder.removeFromSuperview()
    }
}



    
   

//
//  ViewController.swift
//  CodeTest
//
//  Created by Faizul Karim on 30/10/21.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var lblResponseView: UITextView!
    
    //MARK:- Class Variable
    
    
    //MARK:- Memory Management Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        print("💥💥💥 ViewController Deinit💥💥💥")
    }


    //MARK:- Custom Method

    func showData(FullString : String) {
        
        let lastCharecter = FullString.replacingOccurrences(of: "\\s", with: "", options: .regularExpression).last
        let tenthCharecter = everyTenthCherecter(str: FullString)
        let words = wordCount(str: FullString)
        self.lblResponseView.text = "1. last character is \(lastCharecter!.description) \n 2. Every 10th character \n \(tenthCharecter) \n\n 3. Word and word count : \n\n \(words)"
    }
    
    func wordCount(str : String) -> String {
        var words: [Substring] = []
        str.enumerateSubstrings(in: str.startIndex..., options: .byWords) { _, range, _, _ in
            words.append(str[range] + " = Total words \(str[range].count) \n")
            
        }
        let wordString = words.joined(separator: ", ").replacingOccurrences(of: ",", with: "")
        return wordString
    }
    func everyTenthCherecter(str : String) -> String {
        let withoutWhiteSpaceString = str.replacingOccurrences(of: "\\s", with: "", options: .regularExpression)
        let first = 9
        let last = withoutWhiteSpaceString.count
        let interval = 10
        let range = first ..< last
        let lazyCollection = range.lazy.filter({ $0 % interval == 0 })
       
        var words = [String]()

        for element in lazyCollection {
            let cherecter = Array(withoutWhiteSpaceString)[element]
            words.append("\(element) character : \(cherecter)\n")
                self.lblResponseView.text = self.lblResponseView.text + "\(element) character : \(cherecter)\n"
            }
            
        
        
        let wordString = words.joined(separator: ", ").replacingOccurrences(of: ",", with: "")
        return wordString
    }


    //MARK:- Action method
    @IBAction func callApi(_ sender: Any) {
        self.apiCalll()
    }
    
    //MARK:- Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
}

extension ViewController{
    
    func apiCalll(){
        
        ApiManager.shared.makeRequest(method: .user(.TermsOfUse) , parameter: [:]) { (result) in
            switch result {
            case .success(let apiData):
                print(apiData.response)
                if !apiData.response.isEmpty{
                self.showData(FullString: apiData.response)
                }
            case .failure(_):
                break
            }


        }
    }
}




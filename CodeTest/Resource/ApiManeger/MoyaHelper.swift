//
//  MoyaHelper.swift
//  CodeTest
//
//  Created by Faizul Karim on 30/10/21.
//

import UIKit
import SwiftyJSON
@_exported import QorumLogs
@_exported import Moya
@_exported import Alamofire
@_exported import QorumLogs


/// Api Environment
enum APIEnvironment {
    case live
    case local
    case localhost
}

/// NetworkManager
struct NetworkManager  {
    let provider = MoyaProvider<ApiManager>(plugins: [NetworkLoggerPlugin(verbose: true)])
    static let environment : APIEnvironment = .localhost

}

/// Moya String Helper Extenstions
extension String: Moya.ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
    

    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}

/// Response
extension Response {
    
    public func filterApiStatusCodes<R: RangeExpression>(statusCodes: R) throws -> Response where R.Bound == Int {
        guard statusCodes.contains(statusCode) else {
            throw MoyaError.statusCode(self)
        }
        return self
    }
}

/// WebService
class WebService {
    // session manager
    static func manager() -> Alamofire.SessionManager {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 40 // as seconds, you can set your request timeout
        configuration.timeoutIntervalForResource = 40 // as seconds, you can set your resource timeout
        let manager = Alamofire.SessionManager(configuration: configuration)
        //        manager.adapter = CustomRequestAdapter()
        
        return manager
    }
        
    // request adpater to add default http header parameter
    private class CustomRequestAdapter: RequestAdapter {
        public func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
            return urlRequest
        }
    }
}

/// ResponseModelType

enum ResponseModelType {
    case dictonary, array
}

/// Helping Methods
extension ApiManager {
    
    func manageDebugRequest(parameters: [String:Any]?) {
        let jsonData = try? JSONSerialization.data(withJSONObject: self.parameters!, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        let Data  : String = jsonString
        QLPlusLine()
        QL1("Parameters")
        QL1(JSON(parameters as Any).dictionaryValue)
        print(JSON(parameters as Any).dictionaryValue)
        QL1("")
        print("URL 🔗: ",baseURL.appendingPathComponent(self.path).absoluteString)
        QL1("")
        print("\n  Parameters 📚: ", Data)
        QL1(Data)
        QLPlusLine()
    }
    
    func manageDebugResponse(encryptedString: String, responseDic: JSON) {
        QLPlusLine()
        QL1("")
        QL1(responseDic)
        QLPlusLine()
    }
    
    func manageErrors(apiName: String, error: Error, isShowAlert: Bool = false) {
        QL4("Error \(error.localizedDescription) in method \(apiName)")
        if isShowAlert {
            GFunction.shared.showSnackBar(error.localizedDescription)
        }
    }
    
    func addLoader()  {
        GFunction.shared.addLoader()
    }
    
    func removeLoader()  {
        GFunction.shared.removeLoader()
    }
    

    
    /// Cancel any request
    func cancelRequest(endpoint : ApiEndPoints)  {
        if let task = (self.requests.filter{ $0.endPoint.methodName == endpoint.methodName }).first{
            task.cancellable.cancel()
        }
    }
}

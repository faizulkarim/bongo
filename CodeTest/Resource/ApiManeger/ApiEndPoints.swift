//
//  ApiEndPoints.swift
//  CodeTest
//
//  Created by Faizul Karim on 30/10/21.
//

import UIKit

/// ApiEndPoints - This will be main api points
enum ApiEndPoints {
    
    case user(User)
    var methodName : String {
        
        switch self {
            
        case .user(let user):
            return  user.rawValue
            
    }    
}

enum User : String {
    case TermsOfUse                             = "tos"
  }
}



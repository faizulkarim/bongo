//
//  ApiKeys.swift
//  CodeTest
//
//  Created by Faizul Karim on 30/10/21.
//

import UIKit


enum ApiKeys {
    case header(ApiHeaderKeys)
    case respsone(ApiResponseKey)
    case statusCode(ApiStatusCode)
    
    var value: String {
        switch self {
        case .header(let key):
            return key.rawValue
        case .respsone(let key):
            return key.rawValue
        case .statusCode(let key):
            return key.rawValue
      }
    }
}

/// Set All keys here
extension ApiKeys {
    

    //MARK:- HeaderKeys
    internal enum ApiHeaderKeys: String {
        case kHeaderAPIKey                       = "api-key"
        case kHeaderAPIKeyValue                  = "apikeyValue"
        case kHeaderToken                        = "headerToken"
        case kapp                                = "app"
    }
    
    //MARK:- API Key Constant
    internal enum ApiResponseKey: String {
        case data                               = "data"
        case message                            = "message"
        case code                               = "code"
    }
    
    //MARK:- APIStatusCodeEnum
    internal enum ApiStatusCode: String {
        case invalidOrFail                      = "401"
        case success                            = "200"

    }
}



